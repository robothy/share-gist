import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class SelectorIOTest {

  private static final int capacity = 1024;

  private static final String[] response = new String[]{"A", "B", "C", "D", "E"};

  // 客户端采用阻塞方式读写数据
  static class Client {
    public static void main(String[] args) throws IOException {
      SocketChannel socketChannel = SocketChannel.open(new InetSocketAddress("127.0.0.1", 9898));

      ByteBuffer buff = ByteBuffer.allocate(capacity);
      Scanner scanner = new Scanner(System.in);

      while (true) {
        System.out.println("Please enter a message or 'Exit' to quit.");
        String input = scanner.nextLine();
        if("exit".equalsIgnoreCase(input)) break;

        buff.put((new Date() + "\n\t" + input).getBytes());
        buff.flip();
        socketChannel.write(buff);
        buff.clear();

        // 阻塞方式
        int size = socketChannel.read(buff);
        System.out.println((new Date() + "\n\t" + new String(buff.array(), 0, size)));
        buff.clear();
      }

      scanner.close();
      socketChannel.close();
    }
  }

  // 服务端采用多路复用 IO 模型读写数据
  static class Server {

    // 需要往客户端写的数据
    private static final Map<SocketChannel, String> dataToWrite = new HashMap<>();

    public static void main(String[] args) throws IOException {

      ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
      serverSocketChannel.configureBlocking(false);
      serverSocketChannel.bind(new InetSocketAddress(9898));
      Selector selector = Selector.open();
      serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
      //存在一个问题，只要启动了客户端，条件就一直成立，除非客户端关闭连接
      while (selector.select() > 0) {
        Set<SelectionKey> selectionKeys = selector.selectedKeys();
        for (SelectionKey selectionKey : selectionKeys) {
          if (selectionKey.isAcceptable()) {
            SocketChannel socketChannel = serverSocketChannel.accept();
            socketChannel.configureBlocking(false);
            // 为新连接注册事件
            socketChannel.register(selector, socketChannel.validOps());
            System.out.println("A client connected to server.");
          } else {
            ByteBuffer buff = ByteBuffer.allocate(capacity);
            SocketChannel socketChannel = (SocketChannel) selectionKey.channel();

            if (selectionKey.isReadable()) {
              StringBuilder msg = new StringBuilder();
              int len;
              while ( (len = socketChannel.read(buff)) > 0) {
                buff.flip();
                msg.append(new String(buff.array(), 0, len));
                buff.clear();
              }

              if(-1 == len) {
                socketChannel.close();
                System.out.println("A client disconnected.");
              }else { // 客户端断开了连接
                System.out.println(msg);
                // 准备往客户端写的数据，下一次通道可写的时候写出去
                String responseStr = response[(int) (Math.random() * 100) % (response.length - 1)];
                dataToWrite.put(socketChannel, responseStr);
              }
            } else if (selectionKey.isWritable() && dataToWrite.containsKey(socketChannel)) { // 通道可写，且有数据需要写出去

              byte[] data = dataToWrite.get(socketChannel).getBytes(StandardCharsets.UTF_8);
              for (int offset = 0; offset < data.length; offset += capacity) { // 避免数据超过了缓冲区的大小
                buff.put(data, offset, Math.min(data.length - offset, capacity));
                buff.flip();
                socketChannel.write(buff);
                buff.clear();
              }
              dataToWrite.remove(socketChannel);
            }
          }
        }
        // 清除事件，避免重复处理
        selectionKeys.clear();
      }
    }
  }

}
